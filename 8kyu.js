enough(10, 5, 5);
// 0, He can fit all 5 passengers
enough(100, 60, 50);
// 10, He can't fit 10 out of 50 waiting

function enough(cap, on, wait) {
  if ( on + wait <= cap )
  return 0;

  if ( on + wait > cap ) 
  var toMany = (on + wait - cap);
  return toMany;
}

// Task 2
// Sam Harris => S.H

// Patrick Feeney => P.F

function abbrevName(name) {
  let nameArr = name.split(' ');
  let firstNameLetter = nameArr[0]
    .split('', 1)
    .join('')
    .toUpperCase();
  let lastNameLetter = nameArr[1]
    .split('', 1)
    .join('')
    .toUpperCase();
    return `${firstNameLetter}.${lastNameLetter}`;
}

abbrevName('Sam Harris');

// Task 3
let str = 'lorem is bla';

function findShort(s){
  let arr = s
    .split(' ')
    .sort( (a, b) => a.length - b.length );
  let shortest = arr[0].length;
  return shortest;
}

findShort(str);

// Task 4
const arrNumbers = [12, -11, 5.2];

function sum (numbers) {
  "use strict";
  if ( !numbers.length ) return 0;
  let sumNumbers = numbers.reduce( ( a, b ) => a + b );
  return sumNumbers;
};

// Task 5
const numbers = [2, 5, 10, 3];

function grow(x){
  let multiply = x.reduce( (a, b) => a * b );
  return multiply;
}

// Task 6
const games = ["3:1", "2:2", "0:1","3:1", "2:2", "0:1","3:1", "2:2", "4:1","3:1"];

function points(games) {
  let points = 0;
  for (let game of games) {
    let newGame = game.split(':');
    
    if (newGame[0] > newGame[1]) points += 3;
    if (newGame[0] < newGame[1]) points += 0;
    if (newGame[0] === newGame[1]) points += 1;
  }
  return points;
}

points(games);

// Task 7
function opposite(number) {
  return ( number < 0 ) ? number * -1 : number * -1;
}

// Task 8
function findSum(n) {
  let result = 0;
  for (let i = 0; i <= n; i++)
    if (i % 3 === 0 || i % 5 === 0) result += i
  return result;
}

// Task 9
function greet(lang) {
  return langs[lang] || langs['english'];
}

var langs = {
'english': 'Welcome',
'czech': 'Vitejte',
'danish': 'Velkomst',
'dutch': 'Welkom',
'estonian': 'Tere tulemast',
'finnish': 'Tervetuloa',
'flemish': 'Welgekomen',
'french': 'Bienvenue',
'german': 'Willkommen',
'irish': 'Failte',
'italian': 'Benvenuto',
'latvian': 'Gaidits',
'lithuanian': 'Laukiamas',
'polish': 'Witamy',
'spanish': 'Bienvenido',
'swedish': 'Valkommen',
'welsh': 'Croeso'
};