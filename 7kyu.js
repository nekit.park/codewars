// Task 1
const queue1 = ["sheep", "sheep", "sheep", "wolf", "sheep", "sheep"];
const queue2 = ["sheep", "sheep", "wolf"];

function warnTheSheep(queue) {
  let wolfIndex = queue.indexOf('wolf');
  if (queue.length - 1 === wolfIndex) return 'Pls go away and stop eating my sheep';
  let troubleSheep = (queue.length - 1) - wolfIndex;
  return `Oi! Sheep number ${troubleSheep}! You are about to be eaten by a wolf!`
}

// Task 2
const num = [1, -4, 7, 12];

function positiveSum(arr) {
  let onlyPositive = arr.filter( a => a > 0 );
  if (!onlyPositive.length) return 0;
  let sum = onlyPositive.reduce( (a, b) => a + b );
  return sum;
}

// Task 3
const arr = [1, -2, 3, -4, 5];
invert([]);

function invert(array) {
  let newArr = array.map( a => {
    return a > 0 ? a * -1 : a * -1;
  })
  return newArr;
}

// Task 4

class Ball {
  constructor(ballType = 'regular') {
    this.ballType = ballType;
  }
}

const newBall = new Ball('super');
const newBall1 = new Ball();

// console.log(newBall)
// console.log(newBall1)

// Task 5
const mix = ['1', 3, '5', '-2', 7];

function sumMix(x){
  let result = x.reduce( (a, b) => (+a) + (+b) );
  return +result;
}

// Task 6
divisibleBy([1, 2, 3, 4, 5, 6], 2)

function divisibleBy(array, divisor) {
  return array.filter( a => a % divisor === 0 );
}

// Task 7
function checkCoupon(enteredCode, correctCode, currentDate, expirationDate){
  return enteredCode === correctCode && Date.parse(expirationDate) >= Date.parse(currentDate)
}

checkCoupon("123", "123", "July 9, 2015", "July 1, 2015")
checkCoupon("123", "123", "July 9, 2015", "July 2, 2015")

// Task 8

function angle(n) {
  return 180 * ( n - 2 );
}

// Task 9
function nbYear(p0, percent, aug, p) {
  percent = percent / 100;
  let n = 0;
  while ( p0 < p ) {
    p0 += Math.round(p0 * percent) + aug;
    console.log(p0);
    n++;
  }
  return n;
}

// Task 10
let str1 = 'abrooaciiadaueeeebra';
getCount(str1);

function getCount(str) {
  let vowelsCount = 0;
  const vowels = ['a', 'e', 'i', 'o', 'u'];
  const strArr = str.split('');

  for ( let letter of vowels ) {
    for ( let item of strArr ) {
      if ( letter === item ) vowelsCount++
    }
  }
  return vowelsCount;
}

// Task 11
const a = [44, 75];
const b = [1936, 5626];

function comp(array1, array2) {
  if( !array1 || !array2 ) return false;
  array1.sort(( a, b ) => a - b);
  array2.sort(( a, b ) => a - b);
  return array1.map(num1 => Math.pow(num1, 2)).every((num1, index) => num1 == array2[index]);
}
comp(a, b)

// Task 12
console.log(iqTest("1 3 5 2 9"));


function iqTest(numbers){
  numbers = numbers.split(' ').map(num => +num);
  
  let isEven = (numbers.filter(num => num % 2 === 0).length === 1) ? true : false;

  if (isEven)
    return numbers.indexOf(numbers.filter(num => num % 2 === 0)[0]) + 1;
  return numbers.indexOf(numbers.filter(num => num % 2 !== 0)[0]) + 1;
}

// Task 13
// "din"      =>  "((("
// "recede"   =>  "()()()"
// "Success"  =>  ")())())"
// "(( @"     =>  "))((" 

function duplicateEncode(word){
    let wordArr = word.toLowerCase().split('');
    let output = '';
    for (let letter of wordArr) {
      let first = wordArr.indexOf(letter);
      let last = wordArr.lastIndexOf(letter);
      (first === last) ? output += '(' : output += ')';
    }
    return output;
}
// Task 14
// getEvenNumbers([2,4,5,6]) // should == [2,4,6]

function getEvenNumbers(numbersArray){
  return numbersArray.filter( num => num % 2 === 0 );
}
// Task 15
function nbMonths(oldCar, newCar, salary, percent) {
  let cash = 0,
  month = 1 ;
  while (cash + oldCar <= newCar) {
    if (month % 2 == 0) percent += 0.5;
    oldCar = oldCar - (oldCar *  percent / 100);
    newCar = newCar - (newCar * percent /100);
    cash += salary;
    month++
  }
  return [month -1 ,Math.round(cash - newCar + oldCar)];
}
console.log(nbMonths(2000, 8000, 1000, 1.5));

