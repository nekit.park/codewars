// Task 1

function persistence(num) {
  var times = 0;
  
  num = num.toString();
  
  while (num.length > 1) {
    times++;
    num = num.split('').map(Number).reduce((a, b) => a * b).toString();
  }
  
  return times;
}
console.log(persistence(1234))

// Task 2

function last(xs) {
  return xs.length ? xs.pop() : null;
}

//Task 3
// ("103 123 4444 99 2000"), "2000 103 123 4444 99"
let strng = "2000 10003 1234000 44444444 9999 11 11 22 123";
// ("2000 10003 1234000 44444444 9999 11 11 22 123"), "11 11 2000 10003 22 123 1234000 44444444 9999"

function orderWeight(strng) {
  let weight = strng.split(' ');
  weight.sort(compare);
  return weight.join(' ');
}
function compare( a,b ) {
  let splitA = a.split('');
  let splitB = b.split('');

  splitA = splitA.map(Number);
  splitB = splitB.map(Number);

  let sumA = splitA.reduce( (a,b) => a + b );
  let sumB = splitB.reduce( (a,b) => a + b );

  if (sumA > sumB) return 1;
  if (sumA < sumB) return -1;
  if (sumA === sumB) {
    if (splitA.toString() > splitB.toString()) return 1;
    if (splitA.toString() < splitB.toString()) return -1;
  };

}
orderWeight(strng);
// Task 4
// createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) // => returns "(123) 456-7890"

let phone = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
createPhoneNumber(phone);
function createPhoneNumber(numbers){
  numbers.join('').split('');
  numbers[0] = '(' + numbers[0];
  numbers[2] += ') ';
  numbers[5] += '-';
  numbers = numbers.join('');
  return numbers;
}

// Task 5
function countSheep(num){
  let str = '';
  let count = 1;
  while (count <= num) {
    str += `${count} sheep...`
    count++
  }
  return str;
}

console.log(countSheep(5));

// Task 6
function multiple(x){
  if (x % 3 === 0 && x % 5 === 0) return 'BangBoom';
  if (x % 3 === 0) return 'Bang';
  if (x % 5 === 0) return 'Boom';
  return 'Miss';
}

console.log(multiple(15))
// Task 7
// balance("!!","??") === "Right"
// balance("!??","?!!") === "Left"
// balance("!?!!","?!?") === "Left"
// balance("!!???!????","??!!?!!!!!!!") === "Balance"

function balance(left,right){
 let leftArr = left.split('');
 let rightArr = right.split('');
 let leftCounter = 0;
 let rightCounter = 0;
 
 leftArr.forEach( elem => {
   if (elem === '!') leftCounter += 2;
   if (elem === '?') leftCounter += 3;
 })

 rightArr.forEach( elem => {
   if (elem === '!') rightCounter += 2;
   if (elem === '?') rightCounter += 3;
 })

 if (leftCounter > rightCounter) return 'Left';
 if (leftCounter < rightCounter) return 'Right';
 if (leftCounter === rightCounter) return 'Balance';
}

console.log(balance("!!???!????","??!!?!!!!!!!"));

// Task 8
function narcissistic(value) {
  let numArr = value.toString().split('');
  let output = 0;
  let power = numArr.length;
  for (let number of numArr) {
    output += number ** power;
  }
  return value === output ? true : false;
}
console.log(narcissistic(153));

// Task 9
// digital_root(16)
// => 1 + 6
// => 7

function digital_root(n) {
  let arr = String(n).split('').map(Number);
  while (arr.length > 1) {
    let num = arr.reduce( (a,b) => a + b );
    arr = String(num).split('').map(Number);
  }
  return arr[0];
}

console.log(digital_root(16));

// Task 10
/*
toCamelCase("the-stealth-warrior") // returns "theStealthWarrior"
toCamelCase("The_Stealth_Warrior") // returns "TheStealthWarrior"
*/

function toCamelCase(str){
  let strArr = str.includes('-') ? str.split('-') : str.split('_');
    return strArr
    .map( (str, index) => index != 0 ? str.charAt(0).toUpperCase() + str.slice(1) : str)
    .join('');
};

console.log(toCamelCase("the_stealth_warrior"));

// Task 11
let numArr = [];

console.log(arraySum([1, 2, [1, 2, [1, 2, 3]]]));

function arraySum(arr) {
  let output = 0;
  for (let item of arr) {
    if (Array.isArray(item)) {
      arraySum(item);
    } else {
      if (typeof item == 'number') numArr.push(item);
    }
  }
  console.log(numArr)
  output = numArr.reduce( (a,b) => a + b );
  return output;
};

// Task 12
console.log(countBits(0));

function countBits(n) {
  return n > 0 ? n.toString(2).split('').map(Number).map(n => n === 1).reduce( (a,b) => a + b) : 0;
};

//Task 13
function argCounter(...rest) {
  return rest.length;
}

console.log(argCounter(["foo", "bar"]));